from django.shortcuts import render, get_object_or_404
from .models import Discover, Packages
def discover(request):
	discovers=Discover.objects
	return render(request, 'discover/discover.html', {'discover':discovers})

def detail(request, discover_id):
	details = get_object_or_404(Discover, pk=discover_id)
	return render(request, 'discover/detail.html', {'discover':details})

def package(request):
	packages=Packages.objects
	return render(request, 'discover/detail.html', {'discover':packages})

 