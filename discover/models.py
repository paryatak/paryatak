from django.db import models


class Discover(models.Model):
	
	title = models.CharField(max_length=200)
	pub_date= models.DateTimeField()
	body= models.TextField()
	image= models.ImageField(upload_to='images/')
	image1= models.ImageField(upload_to='images/')
	image2= models.ImageField(upload_to='images/')
	history= models.TextField()
	see= models.TextField()


	def summary(self):
		return self.body[:400]
	def __str__(self):
		return self.title
	def pub_date_stan(self):
		return self.pub_date.strftime('%b %d %Y')

class Packages(models.Model):

	title1 = models.CharField(max_length=200)
	
	def __str__(self):
		return self.title1	