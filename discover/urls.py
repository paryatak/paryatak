from django.urls import path
from . import views

urlpatterns = [

		path('', views.discover, name='discover'),
		path('<int:discover_id>/', views.detail, name='detail'),
]