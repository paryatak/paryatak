
from django.contrib import admin

from .models import Discover, Packages

admin.site.register(Discover)

admin.site.register(Packages)
