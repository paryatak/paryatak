from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from users import views as user_views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.auth import views as auth_views




urlpatterns = [

   url('admin/', admin.site.urls),
   url('register/',user_views.register,name='register'),
   url('profile/',user_views.profile,name='profile'),
   url('login/',auth_views.LoginView.as_view(template_name='users/login.html'),name='login'),
   url('logout/',auth_views.LogoutView.as_view(template_name='users/logout.html'),name='logout'),
   url(r'', include('paryatak.urls')),
   url('discover/',include('discover.urls')),
] +static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

