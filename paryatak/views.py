# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import *
from django.shortcuts import render
from .filters import BusFilter
from django.views.generic import TemplateView, DetailView, View
from django.db.models import Q
from django.core.mail import EmailMultiAlternatives
def home(request):
    print('inside home view')
    return render(request, 'paryatak/home.html')


def about(request):
    print('inside about view')
    return render(request, 'paryatak/about.html')

def login(request):
    print('inside about view')
    return render(request, 'paryatak/login.html')

def contact(request):
    print('inside about view')
    return render(request, 'paryatak/contact.html')


# def search(request):
#   bus_list = BusTravel.objects.all()
#   bus_filter = BusFilter(request.GET, queryset=bus_list)
#   return render(request, 'paryatak/search.html',{'filter': bus_filter})


def seat17(request):
    print('inside about view')
    return render(request, 'paryatak/seats.html')

class BusListView(TemplateView):
    template_name = 'paryatak/search.html'
    context_object_name = 'buses'

    def get(self, request, *args, **kwargs):
        departure_time = request.GET.get('departure_time', '')
        Destination_from = request.GET.get('Destination_from', '')
        Destination_to = request.GET.get('Destination_to', '')
        q = {
            'departure_time': departure_time,
            'Destination_from': Destination_from,
            'Destination_to': Destination_to
        };
        print('printing q >>', q);
        if (q != None and q['departure_time'] != '' and q['Destination_from'] != '' and q['Destination_to'] != ''):
            print('In if statement')
            buses = BusTravel.objects.filter(Q(
                destination_from__icontains=q['Destination_from']
            ) & (Q(
                destination_to__icontains=q['Destination_to'])
            )&Q(
            departure_time__icontains=q['departure_time']
            ))
        else:
            print('In else statement >>>>>');
            buses = BusTravel.objects.all()


        return render(request,self.template_name, {'buses':buses})

class BusDetailView(DetailView):
    model = BusTravel
    template_name = 'paryatak/bus_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        bus_travel_seats = BusTravelSeat.objects.filter(bus_travel=self)

        # context['seats']=bustravelseat_id
        return context


def bus_detail(request, pk):
    bus_travel = BusTravel.objects.get(id=pk)
    bus_travel_seats = BusTravelSeat.objects.filter(bus_travel=bus_travel).order_by('id')
    bus_name = bus_travel.bus.bus_no
    print (len(bus_name))
    bus_travel_seats_data = []
    for bus_travel_seat in bus_travel_seats:
        bus_travel_seat_data = {}
        bus_travel_seat_data['id'] = bus_travel_seat.id
        full_name = bus_travel_seat.seat.name
        slice_data = slice(len(bus_name)+1, len(full_name))
        name = full_name[slice_data]
        bus_travel_seat_data['name'] = name
        bus_travel_seat_data['is_available'] = bus_travel_seat.is_available
        bus_travel_seats_data.append(bus_travel_seat_data)
    print (bus_travel_seats_data)
    return render(request, 'paryatak/bus_detail.html', {
        'bus_travel': bus_travel,
        'bus_travel_seats': bus_travel_seats_data
    })

class SeatBookView(View):
    def get(self,request,*args,**kwargs):
        seat_id = self.kwargs.get('sa')
        bustravel_id = self.kwargs.get('pk')
        user = request.user
        customer_profile = Profile.objects.get(
            user=user
        )
        obj = BusTravelSeat.objects.get(id=seat_id)
        obj.is_available='False'
        obj.customer= customer_profile
        obj.save()
        bus_name = obj.bus_travel.bus
        seat_name = obj.seat.name
        subject,from_email,to = 'Parytak:Booking confirmed',settings.EMAIL_HOST_USER, user.email
        text_content='Hello, {}. your seat {} in bus {} for {} is booked. If you have any enquiries, contact us.'.format(user.username,obj.seat,obj.bus_travel,obj.booked_time)
        msg = EmailMultiAlternatives(subject,text_content,from_email,[to])
        msg.send()
        return redirect ('paryatak:bus_detail', pk=bustravel_id)


class TicketListView(TemplateView):
    template_name = 'paryatak/searchtickets.html'
    model = BusTravelSeat

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        print(self.request.user.profile,'\n\n\n\n')
        context['tickets']=BusTravelSeat.objects.filter(customer=self.request.user.profile)
        return context


class cancelho(View):
    def get(self,request,*args,**kwargs):
        seat_id = self.kwargs.get('pa')
        user = request.user
        obj = BusTravelSeat.objects.get(id=seat_id)
        obj.is_available='True'
        obj.customer= None
        obj.save()
        bus_name = obj.bus_travel.bus
        seat_name = obj.seat.name
        subject,from_email,to = 'Parytak:Booking cancelled',settings.EMAIL_HOST_USER, user.email
        text_content='Hello, {}. your seat {} in bus {} for {} is been cancelled. If you have any enquiries, contact us.'.format(user.username,obj.seat,obj.bus_travel,obj.booked_time)
        msg = EmailMultiAlternatives(subject,text_content,from_email,[to])
        msg.send()
        return redirect ('paryatak:search')
