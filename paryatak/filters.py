from .models import BusTravel
import django_filters

class BusFilter(django_filters.FilterSet):
    class Meta:
        model = BusTravel
        fields = ['destination_from', 'destination_to', 'departure_time', ]