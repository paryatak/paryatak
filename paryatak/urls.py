from django.conf.urls import url
from. import views

app_name='paryatak'

urlpatterns = [
    url(
        r'seat17/$',
        views.seat17,
        name='book-seat17'
    ),
    url(
        r'home/$',
        views.home,
        name='book-home'
    ),
    url(
        r'home/about/',
        views.about,
        name='book-about'
    ),
    url(
        r'home/login/',
        views.login,
        name='book-login'
    ),
    url(
        r'home/contact/',
        views.contact,
        name='book-contact'
    ),
    url(
        r'searchnew/$',
        views.BusListView.as_view(),
        name='search'    
    ),
    url(
        r'searchtickets/$',
        views.TicketListView.as_view(),
        name='search_tickets'
    ),
    url(
        r'searchnew/(?P<pk>\d+)/detail/$',
        views.bus_detail,
        name='bus_detail'
    ),
    url(
        r'searchnew/(?P<pk>\d+)/detail/(?P<sa>\d+)/$',
        views.SeatBookView.as_view(),
        name='samrat'
    ),
    url(
        r'searchtickets/(?P<pa>\d+)/$',
        views.cancelho.as_view(),
        name='samrat2'

    )
]  

