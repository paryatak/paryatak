# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import Bus, BusTravel, Seat, BusTravelSeat
from django.contrib import admin

# Register your models here.
class BusAdmin(admin.ModelAdmin):
    list_display = ['bus_no', 'price', 'vehical_type']
  

    def save_model(self, request, obj, form, change):
        super(BusAdmin, self).save_model(request, obj, form, change)
        bus_number = obj.bus_no
        number_of_seats = obj.capacity
        for i in range(1, number_of_seats+1):
            name = str(bus_number)+ '_' +str(i)
            Seat.objects.create(bus=obj, name=name)  

admin.site.register(Bus,BusAdmin)


class BusTravelAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        super(BusTravelAdmin,self).save_model(request, obj, form, change)
        bus=obj.bus
        seats=Seat.objects.filter(bus=bus)
        for seat in seats:
            BusTravelSeat.objects.create(bus_travel=obj, seat=seat, is_available=True)
      

admin.site.register(BusTravel,BusTravelAdmin)
admin.site.register(Seat)
admin.site.register(BusTravelSeat)