# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone	
from users.models import *



class Bus(models.Model):
    no_of_seat = (
        (17, '17'),
        (25, '25'),
        (29, '29')
    )
    type_vehicle = (
        ('MICRO', 'MICRO'),
        ('BUS', 'BUS'),
        ('DEALUX', 'DEALUX')
    )
    capacity = models.IntegerField(
        choices=no_of_seat
    )
    bus_no = models.CharField(
        max_length=15
    )
    price = models.DecimalField(
        decimal_places=5,
        max_digits=10,
    )
    vehical_type = models.CharField(
        max_length=100,
        choices=type_vehicle
    )

    def __str__(self):
        return self.bus_no


class Seat(models.Model):
    bus = models.ForeignKey(
        Bus,
        on_delete=models.CASCADE,
        related_name="seats"
    )
    name = models.CharField(
        max_length=50
    )

    def __str__(self):
        return self.name


class BusTravel(models.Model):
    dest_from = (
        ('CHITWAN', 'CHITWAN'),
        ('KATHMANDU', 'KATHMANDU'),
        ('POKHARA', 'POKHARA'),
        ('JHAPA', 'JHAPA'),
    )
    dest_to = (
        ('CHITWAN', 'CHITWAN'),
        ('KATHMANDU', 'KATHMANDU'),
        ('POKHARA', 'POKHARA'),
        ('JHAPA', 'JHAPA'),
    )
    bus = models.ForeignKey(Bus,
        on_delete=models.CASCADE
    )
    destination_from = models.CharField(
        max_length=100,
        choices=dest_from
    )
    destination_to = models.CharField(
        max_length=100,
        choices=dest_to
    )
    departure_time = models.DateTimeField(auto_now=False)

    def __str__(self):
        return self.bus.bus_no


class BusTravelSeat(models.Model):
    bus_travel = models.ForeignKey(
        BusTravel,
        on_delete=models.CASCADE,
        related_name='bustravelseats'
    )
    seat = models.ForeignKey(
        Seat,
        on_delete=models.CASCADE
    )
    is_available = models.BooleanField(default=True)
    booked_time = models.DateTimeField(auto_now=True)
    customer = models.ForeignKey(
        Profile,
        on_delete=models.CASCADE,
        null=True  
    )